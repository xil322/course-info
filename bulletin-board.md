# CSE 298 - Summer 2021 - Bulletin Board

## :computer: Homework

- [ ] 07/19 - [Homework 2](https://gitlab.com/lehigh-cse-298/summer-2021/assignments/homework-2) - Matlab Exercises
- [x] 07/09 - [Homework 1](https://gitlab.com/lehigh-cse-298/summer-2021/assignments/homework-1) - Learning Git
- [x] 07/09 - [Homework 0](https://gitlab.com/lehigh-cse-298/summer-2021/course-info/-/blob/master/Homework0.md) - Sign up for Gitlab

## :checkered_flag: Quizzes and Exams

- [ ] 07/14 - [Quiz 1](https://gitlab.com/lehigh-cse-298/summer-2021/assignments/quiz-1)

## :books: Readings

| Week                      | Readings           | 
| ------------------------- | ------------------ | 
| Week 1 | [The Missing Semester](https://missing.csail.mit.edu) - Chapters 1, 2, 5, 6 |

## :vhs: Lectures - [Playlist](https://youtube.com/playlist?list=PL4A2v89SXU3TS1fcYFmXi-tch8p0LucE7)

| Item                      | Date              | Content          | Links          |
| ------------------------- | ------------------ | ------------------ | --------------- |
|**Week 1**|
|Recitation 1 | 7/8 | Recitation 1 | [Video](https://drive.google.com/drive/folders/1Z8K2o0iNE70arJC8uYm2LWnEYu2NqqNG?usp=sharing)
|Lecture 2 | 7/7 | Sensors | [Video](https://youtu.be/dLsOutDHlF4)
|Lecture 1 | 7/6 | Perception | [Video](https://youtu.be/09RD2dNwQew)
